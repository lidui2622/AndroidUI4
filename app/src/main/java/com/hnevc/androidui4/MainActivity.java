package com.hnevc.androidui4;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RadioGroup;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    String[] fruits=new String[]{"苹果","榴莲","西瓜"};
    class MyButtonListener implements View.OnClickListener{

        @Override
        public void onClick(View v) {
            Toast.makeText(MainActivity.this
                    , "内部类点击事件处理"
                    , Toast.LENGTH_SHORT)
                    .show();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button btnMyDialog = findViewById(R.id.btn_mydialog);
        btnMyDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //使用对话框
                MyDialog dialog = new MyDialog(MainActivity.this);
                dialog.show();
            }
        });


        Button btnProgressDialog = findViewById(R.id.btn_progress_dialog);
        btnProgressDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ProgressDialog progressDialog = new ProgressDialog(MainActivity.this);
                progressDialog.setMessage("加载中....");
                progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progressDialog.show();
                //progressDialog.dismiss();

            }
        });



        //1
        Button button1 = findViewById(R.id.btn_button1);//获取空间控件
        //2
        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(MainActivity.this
                        , "匿名内部类对象"
                        , Toast.LENGTH_SHORT).show();

            }
        });
        Button button2 = findViewById(R.id.btn_button2);//获取控件
        button2.setOnClickListener(new MyButtonListener());


        //RodioButton Rodio group
        RadioGroup rgSex = findViewById(R.id.rg_sex);
        rgSex.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                Toast.makeText(MainActivity.this, checkedId+"", Toast.LENGTH_SHORT).show();
                if(checkedId == R.id.rb_male){
                    Toast.makeText(MainActivity.this, "男", Toast.LENGTH_LONG).show();
                }
                if (checkedId == R.id.rb_female){
                    Toast.makeText(MainActivity.this, "女", Toast.LENGTH_SHORT).show();
                }
            }
        });

        Button btnAlertDialog = findViewById(R.id.btn_alert_dialog);
        btnAlertDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //构建对话框，显示对话框
                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                //利用builer设置标题
                builder.setTitle("警告框");
                //这是消息
                //builder.setMessage("你是否真的确认要删除吗");
                /*builder.setSingleChoiceItems(fruits, -1, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        Toast.makeText(MainActivity.this, fruits[which], Toast.LENGTH_SHORT).show();
                    }
                });*/

                builder.setMultiChoiceItems(fruits, null, new DialogInterface.OnMultiChoiceClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which, boolean isChecked) {
                        Toast.makeText(MainActivity.this, fruits[which]+isChecked, Toast.LENGTH_SHORT).show();
                    }
                });

                builder.setPositiveButton("确认", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Toast.makeText(MainActivity.this, "确认被点击", Toast.LENGTH_SHORT).show();
                    }
                });
                builder.setNegativeButton("取消", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Toast.makeText(MainActivity.this, "取消被点击", Toast.LENGTH_SHORT).show();
                    }
                });
                //创建对话框
                AlertDialog dialog = builder.create();

                dialog.show();
            }
        });

    }


    public void click(View view) {
        Toast.makeText(this, "按钮被点击", Toast.LENGTH_SHORT).show();
    }
}
